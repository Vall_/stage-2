import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import App from '../../app';

export function showWinnerModal(fighter) {
  const title = `Winner: ${fighter.name}`;
  const bodyElement = createElement({
    tagName: 'img',
    className: 'modal-body',
    attributes: {
      src: fighter.source,
    },
  });
  showModal({
    title,
    bodyElement,
    onClose: () => {
      document.getElementById('root').innerHTML = '';
      new App();
    },
  });
}
