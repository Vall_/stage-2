import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  const fighterImage = createFighterImage(fighter);
  const fighterInformation = createFighterInformation(fighter);

  fighterElement.append(fighterInformation);
  fighterElement.append(fighterImage);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

const createFighterInformation = ({ name, ...otherFighterData }) => {
  const root = createElement({
    tagName: 'div',
    className: 'fighter-preview___information',
  });
  const fighterName = createElement({ tagName: 'h4', className: 'fighter-preview___name' });

  fighterName.textContent = name;

  root.append(fighterName);

  ['health', 'defense', 'attack'].forEach((characteristic) => {
    const characteristicParagraph = createElement({ tagName: 'p', className: 'fighter-preview___characteristic' });

    characteristicParagraph.textContent = `${characteristic}: ${otherFighterData[characteristic]}`;

    root.append(characteristicParagraph);
  });

  return root;
};
