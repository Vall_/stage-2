import { controls } from '../../constants/controls';
import { arenaService } from '../services/arenaService';
import { generateRandomNumber } from '../helpers/numberHelper';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const leftFighterHealthIndicator = document.getElementById('left-fighter-indicator');
    const rightFighterHealthIndicator = document.getElementById('right-fighter-indicator');

    const leftOnePercent = leftFighterHealthIndicator.offsetWidth / firstFighter.health;
    const rightOnePercent = rightFighterHealthIndicator.offsetWidth / secondFighter.health;

    let firstFighterHealth = firstFighter.health;
    let secondFighterHealth = secondFighter.health;

    const keysCache = {};

    window.addEventListener('keyup', (event) => {
      delete keysCache[event.code];
    });

    window.addEventListener('keydown', (event) => {
      if (event.code in keysCache) return;
      keysCache[event.code] = event.code;

      if (keysCache[controls.PlayerOneBlock] || keysCache[controls.PlayerTwoBlock]) {
        return;
      }

      if (event.code === controls.PlayerOneAttack) {
        const damage = getDamage(firstFighter, secondFighter);

        secondFighterHealth = secondFighterHealth - damage;

        arenaService.showDamage(damage, 'right');

        if (secondFighterHealth <= 0) {
          rightFighterHealthIndicator.style.width = '0%';
          resolve(firstFighter);
        }

        rightFighterHealthIndicator.style.width =
          rightFighterHealthIndicator.offsetWidth - rightOnePercent * damage + 'px';
      }

      if (event.code === controls.PlayerTwoAttack) {
        const damage = getDamage(secondFighter, firstFighter);

        firstFighterHealth = firstFighterHealth - damage;

        arenaService.showDamage(damage, 'left');

        if (firstFighterHealth <= 0) {
          leftFighterHealthIndicator.style.width = '0%';
          resolve(secondFighter);
        }

        leftFighterHealthIndicator.style.width =
          leftFighterHealthIndicator.offsetWidth - leftOnePercent * damage + 'px';
      }
    });
  });
}

export function getDamage(attacker, defender) {
  const power = getHitPower(attacker);
  const block = getBlockPower(defender);

  if (block > power) return 0;

  return power - block;
}

export function getHitPower(fighter) {
  return fighter.attack * generateRandomNumber(1, 2);
}

export function getBlockPower(fighter) {
  return fighter.defense * generateRandomNumber(1, 2);
}
