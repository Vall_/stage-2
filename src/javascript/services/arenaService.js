import { generateRandomNumber } from '../helpers/numberHelper';

class ArenaService {
  showDamage(damage, position) {
    if (!position || !damage) return;

    const fighterPreviewOffsetWidth = document.querySelector('.fighter-preview___img').offsetWidth / 2;
    const fighterPreviewOffsetHeight = document.querySelector('.fighter-preview___img').offsetHeight / 2;
    const fighterRoot = document.querySelector(`.arena___${position}-fighter`);
    const damageText = document.createElement('span');
    damageText.textContent = Math.round(damage);
    damageText.classList.add('arena___damage');
    damageText.style.cssText = `
          top: ${generateRandomNumber(0, fighterPreviewOffsetWidth)}px; 
          left: ${generateRandomNumber(0, fighterPreviewOffsetHeight)}px; 
        `;

    setTimeout(() => {
      damageText.classList.add('arena___damage-hide');
    }, 1000);

    fighterRoot.append(damageText);
  }
}

export const arenaService = new ArenaService();
