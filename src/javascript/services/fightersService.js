import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    let fighterData = null;

    try {
      fighterData = await callApi(`details/fighter/${id}.json`);
    } catch (error) {
      return Promise.reject(error);
    }

    return fighterData;
  }
}

export const fighterService = new FighterService();
